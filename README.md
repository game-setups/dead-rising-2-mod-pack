# Dead Rising 2 mod pack
My mod pack with tweaks to improve the game

## Features
- Raised FOV
- Unlimited Zombrex in Katey's office in the safehouse
- All areas unlocked
- Skip intro logos (credits to [gibbed](http://deadrising2mods.proboards.com/thread/613/mod-remove-introductions))

## Source code explained
- Unpacked/re-packed `datafile.big` with [Gibbed.DeadRising2.Tools](http://svn.gib.me/builds/deadrising2/)
- FOV: changed `FOV = "n"` lines in camera.txt
- Zombrex: Changed scissors to Zombrex dispensor in safehouse.txt
- Areas unlocked: Changed `InitialState = 0` to `InitialState = 1` for all locked doors in location text files